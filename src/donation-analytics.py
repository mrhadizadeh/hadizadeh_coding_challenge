########################################################################
# FILE: donation-analytics.py
# DESCRIPTION:
# Python code for donation analysis.
# Author: Mohammadreza Hadizadeh (mrhadizadeh@gmail.com)
# Feb. 12, 2018
########################################################################
#
#
import pandas as pd
import numpy as np
import datetime as dt

class Preprocess():
    def __init__(self,input_itc,input_prc,cols,names):
        self.input_itc = input_itc
        self.input_prc = input_prc
        self.cols = cols
        self.names = names
    def process(self):
        """ This function preprocesses the data."""
        # reading the input data "itcont.txt" separaed by "|"
        # and keeping the leading zeros in zip codes (by # dtype=str)
        df = pd.read_csv(self.input_itc,sep='|',header=None, dtype=str )
        # changing date format in coulumn 13 to month/day/year
        df.iloc[:,13] = df.iloc[:,13].apply(lambda x:dt.datetime.strptime(str(x),'%m%d%Y'))
        # extracting the columns 0,7,10,13, 14, and 15
        df = df.iloc[:,self.cols]
        df.columns = self.names
        # removing rows with non-empty 'OTHER_ID' field
        df = df[ (df['OTHER_ID'].isnull())]
        # removing rows if 'NAME' is empty
        df = df[pd.notnull(df['NAME'])]
        # removing rows if 'ZIP_CODE' is empty or less than 5 digits
        df = df[pd.notnull(df['ZIP_CODE'])]
        df = df[df['ZIP_CODE'].str.len() >= 5]
        # removing rows if 'TRANSACTION_DT' is empty
        df = df[pd.notnull(df['TRANSACTION_DT'])]
        # keeping the zip code by first 5 digit numbers
        df = df.copy()
        df.loc[:,'ZIP_CODE'] = df.ZIP_CODE.astype(str).str[0:5]
        # keeping 'TRANSACTION_DT' just by year
        df['TRANSACTION_DT'] = df['TRANSACTION_DT'].astype(str).str[0:4]
        # grouping data for same identities having same name and zip code, i.e. same 'NAME' and 'ZIP_CODE' labels, ...
        # ... and droping the duplicates for keeping the last data for the reapeted payments
        df = df.groupby(['NAME','ZIP_CODE']).filter(lambda x: len(x) > 1).drop_duplicates(subset=['NAME','ZIP_CODE'], keep='last')
        # grouping data for same recipient from same zip code, i.e. same 'CMTE_ID' and 'ZIP_CODE' labels, ...
        # ... and removing the groups with one single recipient/zip.
        df = df.groupby(['CMTE_ID','ZIP_CODE']).filter(lambda x: len(x) > 1)
        # index label 'idx' for grouping data for same recipient and from same zip code, i.e. same 'CMTE_ID' and 'ZIP_CODE' labels.
        df['idx'] = pd.Categorical(df['CMTE_ID'].astype(str) + df['ZIP_CODE'].astype(str)).codes
        # adding row index, 'index' label, for each subgroup of repeated donors/recipients
        df['index'] = df.groupby('idx', sort=False).cumcount()+1
        # converting 'TRANSACTION_AMT' column to a numeric type.
        df[['TRANSACTION_AMT']] = df[['TRANSACTION_AMT']].apply(pd.to_numeric, errors='ignore')
        # cumulative sum of 'TRANSACTION_AMT' column, for each subgroup of repeated donors/recipients, in a new column 'TRN_cumsum'
        df['TRN_cumsum'] = df.groupby('idx')['TRANSACTION_AMT'].cumsum()
        return df

    def percentile(self):
        """ Calculation of the percentile """
        df = self.process()
        # reading the percentile value
        prct = pd.read_csv(self.input_prc,header=None, dtype=float)
        # finding [P/100 * N] index using nearest-rank method
        df['prc'] = df.loc[:,'index'].values * prct[0].values / 100. + 0.01
        # round & converting the indices to integer
        df['prc'] = df.prc.apply(np.round).astype(int)
        # if percentile index is 0 ==> 1
        num = df.prc._get_numeric_data()
        num[num <= 0] = 1
        # percentile function to be used for each subgroup of 'idx'
        def percent(x):
            x['Percentile']  = x.TRANSACTION_AMT.iloc[x.prc.values-1].values
            return x
        # calculation of percentile for each subgroup of 'idx', in the new coloumn 'Percentile'
        df = df.groupby('idx').apply(percent)
        # filtering the data by keeping ...
        # 'CMTE_ID','ZIP_CODE','TRANSACTION_DT','Percentile','TRN_cumsum','index' coloumns ...
        # sorted by subgroup index 'idx'
        df_final = df.reset_index().sort_values(['idx','index']).loc[:,['CMTE_ID','ZIP_CODE','TRANSACTION_DT','Percentile','TRN_cumsum','index']]
        # writing the output data in 'repeat_donors.txt'
        df_final.to_csv(r'repeat_donors.txt', header=None, index=None, sep='|')
        return

# Please write the input 'itcont.txt' file name with location
itcont_file = "./input/itcont.txt"

# Please write the percentile file name 'percentile.txt' with location
percentile_file = "./input/percentile.txt"

# the index and labels of columns for ...
# 'CMTE_ID','NAME','ZIP_CODE','TRANSACTION_DT','TRANSACTION_AMT','OTHER_ID'
col_nums = [0,7,10,13,14,15]
col_names = ['CMTE_ID','NAME','ZIP_CODE','TRANSACTION_DT','TRANSACTION_AMT','OTHER_ID']


# Here we call the class we defined with the inputs
preprocessing = Preprocess(itcont_file, percentile_file, col_nums, col_names)

# We call the method that calculates the percentile and ...
# write the ouput file
preprocessing.percentile()
