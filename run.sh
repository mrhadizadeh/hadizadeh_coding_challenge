#!/bin/bash
########################################################################
# FILE: run.sh
# DESCRIPTION:
# Shell script for running donation-analytics.py code
# Author: Mohammadreza Hadizadeh (mrhadizadeh@gmail.com)
# Feb. 12, 2018
########################################################################
#
#
# deleting old output folder
rm -r output
# run the python code
python ./src/donation-analytics.py
# make output directory and move all outputs there
mkdir output
mv *.txt output/
