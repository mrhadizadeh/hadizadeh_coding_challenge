# Analysis of Donations from Individuals

_Created By: Mohammadreza Hadizadeh
<[mrhadizadeh@gmail.com](mailto:mrhadizadeh@gmail.com)>_

_Feb. 11, 2018_

Program for analyzing the individual campaign contributions over multiple years.
The repository contains programs and test calculations.
The input data file listing individual campaign contributions, `percentile.txt` determine which ones came from repeat donors, calculate a few values and distill the results into a single output file, `repeat_donors.txt`.


## Directory Structure

### src

This folder contains a Python source code `donation-analytics.py`.


### input

This folder contains two input data files

1. `itcont.txt`
2. `percentile.txt`

The first input file `itcont.txt` contains the list of individual campaign contributions. This file has 21 columns, including the name, zip code, amount and date of donation, and so on.
[for more details about the structure of the input data, see fec.gov website:
https://classic.fec.gov/finance/disclosure/metadata/DataDictionaryContributionsbyIndividuals.shtml]

The second input file `percentile.txt` contains a single number to be used for percentile computation by **nearest-rank method** [for more details about the method, see Wikipedia: https://en.wikipedia.org/wiki/Percentile].


### output

The created output file of the program, `repeat_donors.txt`, will appear in this folder. The output file has six columns and each cell on each line is separated by a `|`.
The output file `repeat_donors.txt` contains the same number of rows as the input data file, `itcont.txt`, minus any records that were ignored as a result of the 'Input file considerations' and any records you determine did not originate from a repeat donor.

#### Each line of the output file contains these fields:

1. recipient of the contribution (or `CMTE_ID` from the input file)
2. 5-digit zip code of the contributor (or the first five characters of the `ZIP_CODE` field from the input file)
3. 4-digit year of the contribution
4. running percentile of contributions received from repeat donors to a recipient streamed in so far for this zip code and calendar year. Percentile calculations is rounded to the whole dollar.
(anything below $0.50 is dropped and anything from $.50 and up is rounded to the next dollar)
5. total number of transactions received by recipient from the contributor's zip code streamed in so far this calendar year from repeat donors.
6. total amount of contributions received by recipient from the contributor's zip code streamed in so far in this calendar year from repeat donors.


### run.sh

`run.sh` is a shell script for running donation-analytics.py code.


#### To run the code
Use the following command from within the main folder:
```
~$ bash ./run.sh
```


### `insight_testsuite`

This folder contains 2 tests, each one includes the input and output folders, all under the `insight_testsuite/tests` folder. Each test has a separate folder with an `input` folder for `percentile.txt` and `itcont.txt` and an `output` folder for `repeat_donors.txt`.

#### The contents of `insight_testsuite` folder

- `insight_testsuite`
  - `run_tests.sh`
  - `tests`
    - `test_1`
      - `input`
        - `percentile.txt`
        - `itcont.txt`
      - `output`
        - `repeat_donors.txt`
    - `test_2`
      - `input`
        - `percentile.txt`
        - `itcont.txt`
      - `output`
        - `repeat_donors.txt`


#### To run the tests
Use the following command from within the `insight_testsuite` folder:
```
insight_testsuite~$ bash ./run_tests.sh
```

##### On a failed test
The output of `run_tests.sh` looks like:
```
[PASS]: test_1 repeat_donors.txt
[FAIL]: test_2
1,2c1,2
< C00384463|02895|2018|333|333|1
< C00384463|02895|2018|333|773|2
---
> C00384516|02895|2018|333|333|1
> C00384516|02895|2018|333|717|2
[Tue Feb 13 00:27:53 EST 2018] 1 of 2 tests passed
```

##### On success
The output of `run_tests.sh` looks like:
```
[PASS]: test_1 repeat_donors.txt
[PASS]: test_2 repeat_donors.txt
[Tue Feb 13 00:37:00 EST 2018] 2 of 2 tests passed
```
